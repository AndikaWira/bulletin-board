<html>
  <head>
    <title>@yield('title')</title>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/tmdrPreset.css')}}">
    <!-- CSS End -->
    <!-- Javascript -->
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Javascript End -->
  </head>

  <body class="bg-lgray">
    <header>
      @include('messages.layout.header')
    </header>
	
	@yield('content')	

	@yield('modals')

	<footer>
      @include('messages.layout.footer')
    </footer>

	<script>
		// SHOW MODAL
		@if(session('modal'))
		    $(document).ready(function(){
		      $('{{session('modal')}}').modal('show');
		    });	
	  	@endif
	  	
		// INPUT TYPE FILE
		$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
		    numFiles = input.get(0).files ? input.get(0).files.length : 1,
		    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
		});

		$(document).ready( function() {
		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
		  var input = $(this).parents('.input-group').find(':text'),
		      log = numFiles > 1 ? numFiles + ' files selected' : label;

		  if( input.length ) {
		    input.val(log);
		  } else {
		    if( log ) alert(log);
		  }
		});
		});
    </script>
  </body>
</html>