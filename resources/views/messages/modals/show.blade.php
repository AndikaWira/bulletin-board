@extends('messages.layout.layouts')

@section('title', 'image : ' . $image)

@section('content')
	

	<main>
		<img class="img-responsive img-post" 
              src="/storage/messages/image/{{$image}}">
        <div class="text-center mt-10 mb-10">
        	<a href="{{route('messageIndex')}}" type="button" class="btn btn-primary">Back To Provious Page</a>
        </div>
	</main>	
	
@endsection