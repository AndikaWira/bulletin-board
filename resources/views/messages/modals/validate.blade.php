<div class="modal fade" id="cantActionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">
          <div class="alert alert-danger">
            {{session('error')}}
          </div>
        </h4>
      </div>
      <form action="{{(session('action')) ? route(session('action')) : ''}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{session('data.id')}}">
        <div class="modal-body pad-20">
          <div class="post">
            <div class="clearfix">
              <div class="pull-left">
                <h2 class="mb-5 text-green">{{session('data.title')}}</h2>
              </div>
              <div class="pull-right text-right">
                <p class="text-lgray">{{session('data.created_at')}}</p>
              </div>
            </div>
            <h4 class="mb-20">{{session('data.name')}} <span class="text-id">-</span></h4>
            <p>{!!html_entity_decode(nl2br(session('data.body')))!!}</p>
            <div class="img-box my-10">
              @if(session('data.image'))
                <img class="img-responsive img-post" 
                    src="storage/messages/image/thumbnail/{{session('data.image')}}" 
                    alt="Don't have image">
              @else
                <img class="img-responsive img-post" 
                    src="https://via.placeholder.com/500x500.png?text=Don't+have+image" 
                    alt="image">
              @endif
            </div>
          </div>
          @if(!empty(session('data.password')))
            <div class="form-group p-30">
              <label>Password</label>
              <input type="password" class="form-control" name="password">
            </div>
          @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Back Previous Page</button>
            @if(!empty(session('data.password')))
              <button type="submit" class="btn btn-danger" name="submit">Submit</button>
            @endif
        </div>
      </form>
    </div>
  </div>
</div>