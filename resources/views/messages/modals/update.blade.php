<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
        @if ($errors->update->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->update->all() as $error)
                <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif
      </div> 
      <form action="{{route('messageUpdate')}}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
         <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name="id" value="{{session('data.id') ?? session('oldInput.id')}}">
          </div>
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" 
                  value="{{session('data.name') ?? session('oldInput.name')}}" 
                  name="name">
          </div>
          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" 
                  value="{{session('data.title') ?? session('oldInput.title')}}" 
                  name="title">
          </div>
          <div class="form-group">
            <label>Body</label>
            <textarea rows="5" class="form-control" name="body">{{session('data.body') ?? session('oldInput.body')}}</textarea>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              @if(session('data.image') XOR session('image'))
                <img class="img-responsive" alt="Don't have image" 
                    src="storage/messages/image/thumbnail/{{session('data.image') ?? session('image')}}">
              @else
                <img class="img-responsive img-post" 
                    src="https://via.placeholder.com/500x500.png?text=Don't+have+image" 
                    alt="image">
              @endif
            </div>
            <div class="col-md-8 pl-0">
              <label>Choose image from your computer :</label>
              <div class="input-group">
                <input type="text" class="form-control upload-form" value="" readonly>
                <span class="input-group-btn">
                  <span class="btn btn-default btn-file">
                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                  </span>
                </span>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="delete_image">Delete image
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>