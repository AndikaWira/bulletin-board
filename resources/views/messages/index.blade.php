@extends('messages.layout.layouts')

@section('title', 'Timedoor Challenge 3 - 6')

@section('content')

@isset($messages->nama)
  {{dd($messages->nama)}}
@endisset
<main>
  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
          <div class="text-center">
            <h1 class="text-green mb-30"><b>Level 3 - 6 Challenge</b></h1>
          </div>
          @if ($errors->store->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->store->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form action="{{route('messageStore')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control" name="title" value="{{old('title')}}">
            </div>
            <div class="form-group">
              <label>Body</label>
              <textarea rows="5" class="form-control" name="body">{{old('body')}}</textarea>
            </div>
            <div class="form-group">
              <label>Choose image from your computer :</label>
              <div class="input-group">
                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                <span class="input-group-btn">
                  <span class="btn btn-default btn-file">
                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                  </span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password">
            </div>
            <div class="text-center mt-30 mb-30">
              <button class="btn btn-primary">Submit</button>
            </div>
          </form>
          <hr>
          @foreach($messages as $message)
            <div class="post">
              <div class="clearfix">
                <div class="pull-left">
                  <h2 class="mb-5 text-green">{{$message->title}}</h2>
                </div>
                <div class="pull-right text-right">
                  <p class="text-lgray">{{$message->created_at}}</p>
                </div>
              </div>
              <h4 class="mb-20">{{$message->name}}</h4>
              <p>{!!html_entity_decode(nl2br($message->body))!!}</p>
              <div class="img-box my-10">
                @if($message->image)
                  <a href="{{route('messageShow', ['id' => $message->id])}}">
                    <img class="img-responsive img-post" 
                      src="storage/messages/image/thumbnail/{{$message->image}}" 
                      alt="Don't have image">
                  </a>
                @else
                  <img class="img-responsive img-post" 
                      src="https://via.placeholder.com/300x300.png?text=Don't+have+image" 
                      alt="image">
                @endif
              </div>
                <form class="form-inline mt-50" method="post"> 
                  @csrf
                  <div class="form-group mx-sm-3 mb-2">
                    <input type="hidden" name="id" value="{{$message->id}}" readonly="readonly">
                    <label for="inputPassword2" class="sr-only">Password</label>
                    <input type="password" class="form-control" id="inputPassword2" placeholder="Password" name="password">
                  </div>
                  <button type="submit" class="btn btn-default mb-2" formaction="{{route('messageEdit')}}" value="edit" name="submit" method="pull-left"><i class="fa fa-pencil p-3"></i></button>
                  <button type="submit" class="btn btn-danger mb-2" formaction="{{route('messageDelete')}}" value="delete" name="submit" method="pull-left"><i class="fa fa-trash p-3"></i></button>
                </form>
            </div>
          @endforeach      
          <div class="text-center mt-30">
            <nav>
              {{ $messages->links() }}
            </nav>
          </div>         
        </div>
      </div>
    </div>
  </div>
</main>

@endsection

@section('modals')  
  @include('messages.modals.validate')
  @include('messages.modals.update')
  @include('messages.modals.delete')
@endsection