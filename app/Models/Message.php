<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable          = ['name', 'title', 'body', 'password', 'image'];

    public static $imagePath     = 'public/messages/image/';
    public static $thumbnailPath = 'storage/messages/image/thumbnail/';	
}
