<?php

namespace App\Http\Requests;

use App\Models\Message;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'title'     => 'required|min:10|max:32',
            'body'      => 'required|min:10|max:200',
            'image'     => 'mimes:jpg,jpeg,png,gif|max:1000',
        ];
    }

    protected function failedValidation(Validator $validator)
    {        
        $request = $this->validationData();
        $message = Message::findOrFail($request['id']);
        
        return redirect()->back()
                         ->withErrors($validator, 'update')
                         ->with([
                             'modal'    => '#editModal',
                             'oldInput' => $request,                            
                             'image'    => $message->image; 
                            ]); 
    }

}
