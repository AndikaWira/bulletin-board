<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Message;
use App\Http\Requests\StoreMessage;
use App\Http\Requests\UpdateMessage;

class MessageController extends Controller
{   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::orderBy('id', 'DESC')->paginate(3);

        return view('messages.index', compact('messages'));
    }


    /**
     * Show specific image
     * 
     * @param \App\Models\Message $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Message $message)
    {
        return view('messages.modals.show', ['image' => $message->image]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMessage $request)
    {
        if($request->validated()){
            #  check if the request is a file
            #  if true store image
            if ($request->hasFile('image')) {
                $file       = $request->file('image');
                $randName   = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $storeFile  = $file->storeAs(Message::$imagePath, $randName);

                $this->cropImage($randName);
            }
            
            
            #  instance new model object
            $message        = new Message;
            #  create data using orm
            $message->name  = $request->name;
            $message->title = $request->title;
            $message->body  = $request->body;

            if ($request->password) {
                $message->password = Hash::make($request->password);
            }
            if ($request->hasFile('image')) {
                $message->image    = $randName;
            }

            $message->save();

            return back();
        }
    }
       

    public function edit(Request $request){

        $data    = $this->validation($request->id, $request->password);
        $message = Message::findOrFail($request->id);

        if ($data['status'] ?? $data) {
            
            session(['current_id' => $request->id]);

            return back()->with([
                                'modal' => '#editModal',
                                'data'  => Message::findOrFail($request->id)
                            ]);
        }else{
            return back()->with([
                                'action' => 'messageEdit',
                                'modal'  => '#cantActionModal',
                                'error'  => 'cant edit, ' . $data['error'],
                                'data'   => $message
                            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMessage $request)
    {    
        if($request->validated()){
            
            if ($request->id == session('current_id')) {  

                $message  = Message::findOrFail($request->id);
                $randName = null;
    
                #  check if the request is a file and request to delete image is empty
                #  if true update and delete previous image
                if ($request->hasFile('image') &&
                    empty($request->delete_image)) {
    
                    #  check if id have image or not
                    #  if true delete image file
                    if($message->image){
                        Storage::delete(Message::$imagePath . $message->image);
                        Storage::delete(Message::$thumbnailPath . $message->image);
                    }
    
                    $file      = $request->file('image');
                    $randName  = Str::random(20) . '.' . $file->getClientOriginalExtension();
                    $storeFile = $file->storeAs(Message::$imagePath, $randName);
                    $this->cropImage($randName);
                }   
    
                #  check if request to delete image is isset or not empty
                #  if true delete image file
                if ($request->delete_image &&
                   $message->image) {
    
                    Storage::delete(Message::$imagePath . $message->image);
                    Storage::delete(public_path(Message::$thumbnailPath . $messsage->image));
                }
    
                #  update data using orm
    
                $message->name  = $request->name;
                $message->title = $request->title;
                $message->body  = $request->body;
                $message->image = $randName;            
                
                $message->save();
    
                return back();
            }
    
            return back()->with([
                'modal' => '#cantActionModal',
                'data'  => $message,
                'error' => 'not set password'
            ]);
        }
    }


    public function delete(Request $request)
    {
        $data    = $this->validation($request->id, $request->password);
        $message = Message::findOrFail($request->id);

        if ($data['status'] ?? $data) {
            
            session(['current_id' => $request->id]);

            return back()->with([
                            'modal' => '#deleteModal',
                            'id'    => $request->id
                        ]);
        }else{

            return back()->with([
                            'action' => 'messageDelete',
                            'modal'  => '#cantActionModal',
                            'error'  => 'cant delete, ' . $data['error'],
                            'data'   => $message
                        ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {        
        if ($request->id == session('current_id')) {

            # delete data from database and delete file from storage
            Storage::delete(Message::$imagePath . $message->image);
            $message->delete();

            return back();
        }

        return back()->with([
            'modal' => '#cantActionModal',
            'data'  => $message,
            'error' => 'not set password'
        ]);
    }

    public function validation($id, $password){
        $message = Message::findOrFail($id);        

        if ($message->password) {
            if (Hash::check($password, $message->password)) {
                return true;
            } else {
                $error = 'password do not match';
            }
        } else {
            $error = 'not set password';
        }

        return [
            'status' => false,
            'error'  => $error
        ];
    }

    public function cropImage($filename){
        $manager     = new ImageManager;
        $image       = $manager->make(public_path('storage/messages/image/' . $filename));

        $image->crop(300, 300);
        $image->save(public_path(Message::$thumbnailPath . $filename));
    }
}
