<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MessageController@index')->name('messageIndex');

Route::get('/test', function () {
	return false;
})->name('test');

Route::prefix('/messages')->name('message')->group(function () {

	Route::get('/{message}/show', 'MessageController@show')->name('Show');

	Route::post('/store', 'MessageController@store')->name('Store');

	Route::post('/edit', 'MessageController@edit')->name('Edit');

	Route::post('/delete', 'MessageController@delete')->name('Delete');

	Route::patch('/update', 'MessageController@update')->name('Update');

	Route::delete('/{message}/destroy', 'MessageController@destroy')->name('Destroy');

});